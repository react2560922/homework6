import { createContext, useContext, useState } from 'react'

const ViewContext = createContext()

export const ContextTable = ({ children }) => {
  const [checked, setChecked] = useState(false)

  const toggleView = () => {
    setChecked(!checked)
  }

  return (
    <ViewContext.Provider value={{ checked, toggleView }}>
      {children}
    </ViewContext.Provider>
  )
}

export const useViewContext = () => {
  return useContext(ViewContext)
}
