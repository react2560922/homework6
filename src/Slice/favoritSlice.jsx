import { createSlice } from '@reduxjs/toolkit'

const initialState = JSON.parse(localStorage.getItem('favorites')) || []

const favoritSlice = createSlice({
  name: 'favorit',
  initialState,
  reducers: {
    setFavorit: (state, action) => {
      state.push(action.payload)
      localStorage.setItem('favorites', JSON.stringify(state))
    },
    deleteFavorit: (state, action) => {
      const updatedState = state.filter((item) => item.sku !== action.payload)
      localStorage.setItem('favorites', JSON.stringify(updatedState))
      return updatedState
    },
  },
})

export const { setFavorit, deleteFavorit } = favoritSlice.actions
export const selectFavorit = (state) => state.favorit

export default favoritSlice.reducer
