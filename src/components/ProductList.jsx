import { useSelector } from 'react-redux'
import ModalWindow from './ModalWindow'
import ProductCardTable from './ProductCardTable'
import ProductsCard from './ProductCard'
import { getModal } from '../Slice/modalSlice'
import { useState } from 'react'
import { useViewContext } from '../context/ContextTable'

function ProductsList({ products, id }) {
  const [modalProduct, setModalProduct] = useState(null)

  const { isOpen, typeModal } = useSelector(getModal)

  const { checked } = useViewContext()

  return (
    <>
      <div className="bestsellers-content conteiner">
        {isOpen && typeModal == 'delete' && (
          <ModalWindow type="delete" modalProduct={modalProduct} />
        )}
        {isOpen && typeModal == 'add' && (
          <ModalWindow type="add" modalProduct={modalProduct} />
        )}
        {checked ? (
          <table className="bestsellers-content__list-table">
            <tbody>
              {products.map((productItem) => (
                <tr
                  key={productItem.sku}
                  className="bestsellers-content__item-table"
                >
                  <ProductCardTable
                    productItem={productItem}
                    setModalProduct={setModalProduct}
                    id={id}
                  />
                </tr>
              ))}
            </tbody>
          </table>
        ) : (
          <ul
            className={
              id == 1
                ? 'bestsellers-content__list2'
                : 'bestsellers-content__list'
            }
          >
            {products.map((productItem) => (
              <li key={productItem.sku} className="bestsellers-content__item">
                <ProductsCard
                  productItem={productItem}
                  setModalProduct={setModalProduct}
                  id={id}
                />
              </li>
            ))}
          </ul>
        )}
      </div>
    </>
  )
}

export default ProductsList
