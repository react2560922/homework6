import './ModalFooter.scss'
import Button from '../Button'

function ModalFooter({ firstText, secondaryText, firstClick, secondaryClick }) {
  return (
    <div className="modal__footer">
      {firstText && (
        <Button
          onClick={firstClick}
          className=" modal__footer-btn footer-btn2 btn"
        >
          {firstText}
        </Button>
      )}
      {secondaryText && (
        <Button
          onClick={secondaryClick}
          className=" modal__footer-btn footer-btn1 btn"
        >
          {secondaryText}
        </Button>
      )}
    </div>
  )
}

export default ModalFooter
