import { useFormik } from 'formik'
import './OrderForm.scss'
import * as Yup from 'yup'
import { useDispatch } from 'react-redux'
import { orderUser } from '../Slice/cartSlice'
import { PatternFormat } from 'react-number-format'
import InputForm from './InputForm'

function OrderForm({ productsList }) {
  const dispatch = useDispatch()

  const formik = useFormik({
    initialValues: {
      firstName: '',
      lastName: '',
      age: '',
      address: '',
      phoneNumber: '',
    },

    validationSchema: Yup.object().shape({
      firstName: Yup.string()
        .matches(
          /^[а-яА-ЯіІїЇєЄґҐ']+$/u,
          "Ім'я повинно містити лише букви (українська розкладка)"
        )
        .required("Поле обов'язкове для заповнення"),
      lastName: Yup.string()
        .matches(
          /^[а-яА-ЯіІїЇєЄґҐ']+$/u,
          'Прізвище повинно містити лише букви (українська розкладка)'
        )
        .required("Поле обов'язкове для заповнення"),
      age: Yup.number()
        .typeError('Вік повинен бути числом')
        .integer('Вік повинен бути цілим числом')
        .min(1, 'Вік повинен бути більшим або дорівнювати 1')
        .required("Поле обов'язкове для заповнення"),
      address: Yup.string().required("Поле обов'язкове для заповнення"),
      phoneNumber: Yup.string()
        .matches(
          /^\+3 \(\d{3}\) \d{3}-\d{2}-\d{2}$/,
          'Форма номеру телефону +3 (###) ###-##-##'
        )
        .required("Поле обов'язкове для заповнення"),
    }),

    onSubmit: (values, { resetForm }) => {
      const fullProductsList = { ...values, productsList }

      console.log(fullProductsList)

      dispatch(orderUser(productsList))

      resetForm({
        values: {
          firstName: '',
          lastName: '',
          age: '',
          address: '',
          phoneNumber: '',
        },
      })
    },
  })

  return (
    <form onSubmit={formik.handleSubmit}>
      <label htmlFor="firstName">Введіть ваше Ім'я</label>
      <InputForm
        tag="input"
        name="firstName"
        typeElemForm="text"
        formik={formik}
      />

      <label htmlFor="firstName">Введіть ваше Призвіще</label>
      <InputForm
        tag="input"
        name="lastName"
        typeElemForm="text"
        formik={formik}
      />

      <label htmlFor="firstName">Введіть ваш вік</label>
      <InputForm tag="input" name="age" typeElemForm="text" formik={formik} />

      <label htmlFor="address">Введіть вашу адресу</label>
      <InputForm tag="textarea" name="address" formik={formik} />

      <label htmlFor="phoneNumber">Введіть ваш номер телефону</label>

      <PatternFormat
        name="phoneNumber"
        format="+3 (###) ###-##-##"
        allowEmptyFormatting
        mask="#"
        value={formik.values.phoneNumber}
        onChange={(e) => {
          formik.setFieldValue('phoneNumber', e.target.value)
        }}
      />

      {formik.touched.phoneNumber && formik.errors.phoneNumber && (
        <div className="errors">{formik.errors.phoneNumber}</div>
      )}
      <button type="submit">Оформити замовлення </button>
    </form>
  )
}
export default OrderForm
