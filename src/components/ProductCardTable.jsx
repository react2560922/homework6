import Button from './Button'
import { useState, useEffect, useContext } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { selectCart } from '../Slice/cartSlice'
import { deleteFavorit, selectFavorit, setFavorit } from '../Slice/favoritSlice'
import './ProductCardTable.scss'
import { closeModal, openModal } from '../Slice/modalSlice'

function ProductCardTable({ productItem, setModalProduct, id }) {
  const cartCounter = useSelector(selectCart)
  const favoritesCounter = useSelector(selectFavorit)

  const dispatch = useDispatch()

  const [isActive, setIsActive] = useState(false)
  const [isActiveFav, setIsActiveFav] = useState(false)

  useEffect(() => {
    cartCounter.map((elem) => {
      setIsActive((prevState) => ({ ...prevState, [elem.sku]: true }))
    })
  }, [cartCounter])

  useEffect(() => {
    favoritesCounter.map((elem) => {
      setIsActiveFav((prevState) => ({ ...prevState, [elem.sku]: true }))
    })
  }, [, favoritesCounter])

  const addToCard = (productItem, counter, value) => {
    const isExist = counter.some((obj) => obj.sku === productItem.sku)

    isExist
      ? dispatch(closeModal())
      : (setModalProduct({
          ...productItem,
          value,
        }),
        value == 'cart'
          ? dispatch(openModal({ isOpen: true, typeModal: 'add' }))
          : dispatch(setFavorit(productItem)))
  }

  return (
    <>
      <td>{productItem.name}</td>
      <td>{productItem.price} €</td>
      <td>
        {id == 2 ? (
          <Button
            onClick={() => {
              addToCard(productItem, favoritesCounter, 'favorit')
              id == 2 ? dispatch(deleteFavorit(productItem.sku)) : null
            }}
            className={
              'bestsellers-content__add-to-favorit  active-favorites-table'
            }
          >
            Видалити товар з обраного
          </Button>
        ) : (
          <Button
            onClick={() => {
              addToCard(productItem, favoritesCounter, 'favorit')
              id == 2 ? dispatch(deleteFavorit(productItem.sku)) : null
            }}
            className={
              isActiveFav[productItem.sku]
                ? 'bestsellers-content__add-to-favorit  active-favorites-table'
                : 'bestsellers-content__add-to-favorit'
            }
          >
            {isActiveFav[productItem.sku]
              ? 'Товар додано до обраного'
              : 'Додати товар до обраного'}
          </Button>
        )}
      </td>
      <td>
        {id == 1 ? (
          <Button
            onClick={() => {
              dispatch(openModal({ isOpen: true, typeModal: 'delete' }))
              setModalProduct(productItem)
            }}
            className={'bestsellers-content__add-to-basket-table active-cart'}
          >
            Видалити товар з кошика
          </Button>
        ) : (
          <Button
            onClick={() => {
              addToCard(productItem, cartCounter, 'cart')
            }}
            className={
              isActive[productItem.sku]
                ? 'bestsellers-content__add-to-basket-table active-cart'
                : 'bestsellers-content__add-to-basket-table'
            }
          >
            {isActive[productItem.sku]
              ? 'Товар додано до кошика'
              : 'Додати товар до кошика'}
          </Button>
        )}
      </td>
    </>
  )
}

export default ProductCardTable
