import Button from './Button'
import { useState, useEffect, useContext } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { selectCart } from '../Slice/cartSlice'
import { deleteFavorit, selectFavorit, setFavorit } from '../Slice/favoritSlice'
import ModalClose from './ModalComponents/ModalClose'
import './ProductCard.scss'
import { closeModal, openModal } from '../Slice/modalSlice'

function ProductsCard({ productItem, setModalProduct, id }) {
  const cartCounter = useSelector(selectCart)
  const favoritesCounter = useSelector(selectFavorit)

  const dispatch = useDispatch()

  const [isActive, setIsActive] = useState(false)
  const [isActiveFav, setIsActiveFav] = useState(false)

  useEffect(() => {
    cartCounter.map((elem) => {
      setIsActive((prevState) => ({ ...prevState, [elem.sku]: true }))
    })
  }, [cartCounter])

  useEffect(() => {
    favoritesCounter.map((elem) => {
      setIsActiveFav((prevState) => ({ ...prevState, [elem.sku]: true }))
    })
  }, [, favoritesCounter])

  const addToCard = (productItem, counter, value) => {
    const isExist = counter.some((obj) => obj.sku === productItem.sku)
    isExist
      ? dispatch(closeModal())
      : (setModalProduct({
          ...productItem,
          value,
        }),
        value == 'cart'
          ? dispatch(openModal({ isOpen: true, typeModal: 'add' }))
          : dispatch(setFavorit(productItem)))
  }

  return (
    <>
      <div
        className={
          id == 2 || id == null
            ? 'bestsellers-content__offers2'
            : 'bestsellers-content__offers'
        }
      >
        {id != 1 ? (
          <ModalClose
            id={id}
            className={
              isActiveFav[productItem.sku]
                ? 'bestsellers-content__offers-img active-favorites'
                : 'bestsellers-content__offers-img'
            }
            onClick={() => {
              addToCard(productItem, favoritesCounter, 'favorit', isActiveFav)
              if (id == 2) {
                dispatch(deleteFavorit(productItem.sku))
              }
            }}
          />
        ) : (
          <>
            <ModalClose
              id="2"
              className={
                isActiveFav[productItem.sku]
                  ? 'bestsellers-content__offers-img active-favorites'
                  : 'bestsellers-content__offers-img'
              }
              onClick={() => {
                addToCard(productItem, favoritesCounter, 'favorit', isActiveFav)
              }}
            />
            <ModalClose
              id={id}
              onClick={() => {
                dispatch(openModal({ isOpen: true, typeModal: 'delete' }))
                setModalProduct(productItem)
              }}
            />
          </>
        )}
      </div>

      <img
        src={productItem.imageUrl}
        alt=""
        className="bestsellers-content__link-img"
      />
      <p className="bestsellers-content__link-name">{productItem.name}</p>
      <div className="bestsellers-content__price">
        <p className="price-bestsellers">{productItem.price} €</p>
        {+id === 1 ? null : (
          <Button
            onClick={() => {
              addToCard(productItem, cartCounter, 'cart')
            }}
            className={
              isActive[productItem.sku]
                ? 'bestsellers-content__add-to-basket active-cart'
                : 'bestsellers-content__add-to-basket'
            }
          >
            {isActive[productItem.sku]
              ? 'Товар додано до кошика'
              : 'Додати товар до кошика'}
          </Button>
        )}
      </div>
    </>
  )
}

export default ProductsCard
