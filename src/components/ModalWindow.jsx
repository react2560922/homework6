import Modal from './Modal'
import Header from './ModalComponents/ModalHeader'
import Footer from './ModalComponents/ModalFooter'
import Body from './ModalComponents/ModalBody'
import Wrapper from './ModalComponents/ModalWrapper'
import { useDispatch, useSelector } from 'react-redux'
import './ModalComponents/ModalFooter.scss'
import ModalClose from './ModalComponents/ModalClose'
import { deleteCart, selectCart, setCart } from '../Slice/cartSlice'
import { closeModal, getModal } from '../Slice/modalSlice'
import { selectFavorit, setFavorit } from '../Slice/favoritSlice'

function ModalWindow({ type, modalProduct }) {
  const { value, ...addNewProduct } = modalProduct

  const cartCounter = useSelector(selectCart)

  const dispatch = useDispatch()
  const { isOpen } = useSelector(getModal)

  function handleSecondaryClick(productItem) {
    if (type === 'delete') {
      dispatch(deleteCart(productItem.sku))
    }
    dispatch(closeModal())
  }

  function handleFirstClick() {
    dispatch(closeModal())
  }

  const renderContentModal = () => {
    switch (type) {
      case 'delete':
        return (
          <Body>
            <img
              src={modalProduct.imageUrl}
              className="modal__body-img"
              alt={modalProduct.name}
            />
            <h2 className="modal__body-title">{modalProduct.name} Delete!</h2>
            <p className="modal__body-text">
              By clicking the “Yes, Delete” button, <b>{modalProduct.name}</b>{' '}
              will be deleted.
            </p>
          </Body>
        )
      case 'add':
        return (
          <Body>
            <h2 className="modal__body-title">“{modalProduct.name}”</h2>
            <p className="modal__body-text">
              The product has been added to the {value}
            </p>
          </Body>
        )
      default:
        return null
    }
  }

  const renderFooterModal = () => {
    switch (type) {
      case 'delete':
        return (
          <Footer
            firstText="NO, CANCEL"
            firstClick={handleFirstClick}
            secondaryText="YES, DELETE"
            secondaryClick={() => handleSecondaryClick(modalProduct)}
          />
        )

      case 'add':
        return <Footer firstText={`ADD to ${value}`} firstClick={addProduct} />
      default:
        return null
    }
  }

  const addProduct = () => {
    switch (value) {
      case 'cart':
        const cart = cartCounter.some((obj) => obj.sku === modalProduct.sku)

        !cart && (dispatch(setCart(addNewProduct)), handleFirstClick())
        break
    }
  }

  return isOpen ? (
    <Wrapper
      onClick={(event) => {
        if (event.target.classList.contains('modal__wrapper')) {
          dispatch(closeModal())
        }
      }}
    >
      <Modal>
        <Header>
          <ModalClose
            className="modal__close"
            id="1"
            onClick={() => {
              dispatch(closeModal())
            }}
          />
        </Header>
        {renderContentModal()}
        {renderFooterModal()}
      </Modal>
    </Wrapper>
  ) : null
}

export default ModalWindow
