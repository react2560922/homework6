import './Header.scss'
import logo from '../img/logo.png'
import { Link } from 'react-router-dom'
import { useSelector } from 'react-redux'
import { selectCart } from '../Slice/cartSlice'
import { selectFavorit } from '../Slice/favoritSlice'
import { useViewContext } from '../context/ContextTable'

function Header() {
  const cartCounter = useSelector(selectCart)
  const favoritesCounter = useSelector(selectFavorit)

  const { checked, toggleView } = useViewContext()

  return (
    <div className="header-top">
      <div className="header-top__content conteiner">
        <a href="/" className="header-top__logo">
          <img src={logo} alt="logo" className="header-top__logo-img" />
        </a>
        <div className="checkbox">
          <span>Змінити відображення товару</span>
          <label className="switch">
            <input type="checkbox" checked={checked} onChange={toggleView} />
            <span className="slider round"></span>
          </label>
        </div>

        <div className="header-top__basket">
          <Link
            to="/"
            className="header-top__basket-home "
            type="button"
          ></Link>
          <Link
            to="/favorites"
            className="header-top__basket-favorites"
            type="button"
          >
            <span className="header-top__number">
              {favoritesCounter.length}
            </span>
          </Link>
          <Link to="/cart" className="header-top__basket-img" type="button">
            <span className="header-top__number">{cartCounter.length}</span>
          </Link>
        </div>
      </div>
    </div>
  )
}

export default Header
