import '../style.scss'
import { useSelector } from 'react-redux'
import ProductsList from '../components/ProductList'
import { selectFavorit } from '../Slice/favoritSlice'

function ProductsFavorites({ id }) {
  const favoritesCounter = useSelector(selectFavorit)

  return (
    <>
      <p className="modal__body-text">Товар відсутній</p>
      {favoritesCounter.length === 0 ? (
        <p className="modal__body-text">Товар відсутній</p>
      ) : (
        <ProductsList products={favoritesCounter} id={id} />
      )}
    </>
  )
}

export default ProductsFavorites
