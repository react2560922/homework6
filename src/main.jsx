import React from 'react'
import ReactDOM from 'react-dom/client'
import { RouterProvider } from 'react-router-dom'
import { Provider } from 'react-redux'
import store from './store.js'
import { ContextTable } from './context/ContextTable'
import { Router } from './Router.jsx'

ReactDOM.createRoot(document.getElementById('root')).render(
  <Provider store={store}>
    <ContextTable>
      <RouterProvider router={Router} />
    </ContextTable>
  </Provider>
)
