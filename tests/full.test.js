import { render, screen, fireEvent } from '@testing-library/react'
import '@testing-library/jest-dom'
import { Provider } from 'react-redux'
import store from '../src/store' // Import your Redux store
import CardsBook from '../src/pages/CardsBook'
import ProductsCart from '../src/pages/ProductsCart'
import ProductsFavorites from '../src/pages/ProductsFavorites'
import { ContextTable } from '../src/context/ContextTable'

global.fetch = jest.fn(() =>
  Promise.resolve({
    json: () =>
      Promise.resolve([
        {
          name: 'Тест',
          price: 120.0,
          imageUrl: './books/kobzar.jfif',
          sku: '100001',
          color: 'blue',
        },
      ]),
  })
)

const renderWithProvider = (children) => {
  return render(
    <Provider store={store}>
      <ContextTable>{children}</ContextTable>
    </Provider>
  )
}

describe('fell testing ih project', () => {
  test('should fetch and render books', async () => {
    renderWithProvider(<CardsBook />)
    const userElement = await screen.findByText('Тест')
    expect(userElement).toBeInTheDocument()
  })

  test('open and close modal', async () => {
    const { asFragment } = renderWithProvider(<CardsBook />)

    const buttonElement = await screen.findByText('Додати товар до кошика')
    fireEvent.click(buttonElement)
    screen.debug()
    const newButtonElement = await screen.findByText('ADD to cart')
    fireEvent.click(newButtonElement)

    await screen.findByText('Товар додано до кошика')

    expect(asFragment(<CardsBook />)).toMatchSnapshot()
  })

  test('click button favorits', async () => {
    const { asFragment } = renderWithProvider(<CardsBook />)

    const elementFavorit = await screen.findByRole('img', { name: 'testing' })
    fireEvent.click(elementFavorit)

    expect(elementFavorit).toHaveClass('active-favorites')

    expect(asFragment(<CardsBook />)).toMatchSnapshot()
  })

  test('renders favorit items correctly', () => {
    const { asFragment } = renderWithProvider(<ProductsFavorites />)
    const productElement = screen.getByText('Тест')
    expect(productElement).toBeInTheDocument()

    const deleteButtons = screen.getByRole('img', { name: 'testing' })
    expect(deleteButtons).toBeInTheDocument()

    fireEvent.click(deleteButtons)

    // expect(productElement).not.toBeInTheDocument()

    expect(asFragment(<ProductsFavorites />)).toMatchSnapshot()
  })

  test('renders cart items correctly', () => {
    const { asFragment } = renderWithProvider(<ProductsCart id="1" />)

    const productElement = screen.getByText('Тест')
    expect(productElement).toBeInTheDocument()

    fireEvent.click(screen.getByRole('img', { name: 'testing2' }))

    fireEvent.click(screen.getByText('YES, DELETE'))

    expect(productElement).not.toBeInTheDocument()

    expect(asFragment(<ProductsCart id="1" />)).toMatchSnapshot()
  })
})
