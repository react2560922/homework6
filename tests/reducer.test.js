import { configureStore } from '@reduxjs/toolkit'
import cartReducer, { deleteCart, setCart } from '../src/Slice/cartSlice'
import favoritReducer, {
  deleteFavorit,
  setFavorit,
} from '../src/Slice/favoritSlice.jsx'
import modalReducer, {
  closeModal,
  openModal,
} from '../src/Slice/modalSlice.jsx'
import '@testing-library/jest-dom'

describe('cart reducer', () => {
  let store

  beforeEach(() => {
    store = configureStore({
      reducer: {
        cart: cartReducer,
        favorit: favoritReducer,
        modal: modalReducer,
      },
    })
  })

  test('adds item to cart', () => {
    store.dispatch(setCart({ id: 1, name: 'Test Product', price: 100 }))
    expect(store.getState().cart).toEqual([
      { id: 1, name: 'Test Product', price: 100 },
    ])
    console.log(store.getState())
  })
  test('delete item to cart', () => {
    store.dispatch(deleteCart('1'))
    expect(store.getState().cart).toEqual([])
    console.log(store.getState())
  })
  test('adds item to favorit', () => {
    store.dispatch(setFavorit({ id: 1, name: 'Test Product', price: 100 }))
    expect(store.getState().favorit).toEqual([
      { id: 1, name: 'Test Product', price: 100 },
    ])
    console.log(store.getState())
  })
  test('delete item to favorit', () => {
    store.dispatch(deleteFavorit('1'))
    expect(store.getState().favorit).toEqual([])
    console.log(store.getState())
  })
  test('open modal', () => {
    store.dispatch(openModal({ isOpen: true, typeModal: null }))
    expect(store.getState().modal).toEqual({ isOpen: true, typeModal: null })
    console.log(store.getState())
  })
  test('close modal', () => {
    store.dispatch(closeModal({ isOpen: false, typeModal: '' }))
    expect(store.getState().modal).toEqual({ isOpen: false, typeModal: '' })
    console.log(store.getState())
  })
})
