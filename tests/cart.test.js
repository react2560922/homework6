import React from 'react'
import { fireEvent, render, screen } from '@testing-library/react'
import ProductsCart from '../src/pages/ProductsCart'
import { Provider } from 'react-redux'
import configureStore from 'redux-mock-store'
import { ContextTable } from '../src/context/ContextTable'
import '@testing-library/jest-dom'

const mockStore = configureStore([])

test('renders cart items correctly', () => {
  const initialState = {
    cart: [
      {
        name: 'Тест',
        price: 150.0,
        imageUrl: './books/kobzar.jfif',
        sku: '100001',
        color: 'blue',
        value: 'cart',
      },
      {
        name: 'Тест2',
        price: 120.0,
        imageUrl: './books/kobzar.jfif',
        sku: '100002',
        color: 'blue3',
        value: 'cart',
      },
    ],
    modal: {
      isOpen: false,
      typeModal: '',
    },
    favorit: [
      {
        name: 'Тест',
        price: 150.0,
        imageUrl: './books/kobzar.jfif',
        sku: '100001',
        color: 'blue',
        value: 'cart',
      },
      {
        name: 'Тест2',
        price: 120.0,
        imageUrl: './books/kobzar.jfif',
        sku: '100002',
        color: 'blue3',
        value: 'cart',
      },
    ],
  }

  const store = mockStore(initialState)

  render(
    <Provider store={store}>
      <ContextTable>
        <ProductsCart id="1" />
      </ContextTable>
    </Provider>
  )

  const deleteButtons = screen.getAllByRole('img', { name: 'testing' })

  deleteButtons.forEach((button) => {
    expect(button).toBeInTheDocument()
  })

  if (fireEvent.click(deleteButtons[0])) {
    store.getState().modal = { isOpen: true, typeModal: 'delete' }
  }
  screen.debug()

  const modalDelete = screen.getByText(/DELETE/i)

  expect(modalDelete).toBeInTheDocument()
})
